"use strict"

function filterBy(array,type) {
     return array.filter(el => typeof(el) !== type);
}
console.log(filterBy([2, "2", null, 5, "afg", true],'number'));